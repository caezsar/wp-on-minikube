#!/bin/bash

# by Caezsar M.

set -eao pipefail

deploy_cluster() {
# KVM installation and config not covered
    dnf -y install epel-release
    dnf -y install nano vim curl jq wget podman podman-docker bind-utils conntrack
    systemctl disable --now firewalld.service
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    install minikube-linux-amd64 /usr/local/bin/minikube
    wget https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2
    mv docker-machine-driver-kvm2 /usr/local/sbin/
    chmod 755 /usr/local/sbin/docker-machine-driver-kvm2
    curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get-helm-3 | bash

    minikube start --memory=2048 --cpus=2 --driver=kvm2 --force
    minikube addons enable ingress
    minikube addons enable helm-tiller

    alias kubectl="minikube kubectl --"

    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm install wordpress bitnami/wordpress

    minikube kubectl -- delete svc wordpress
    minikube kubectl -- expose deployment wordpress --type=NodePort --port=8080


cat <<EOF> /tmp/wp-ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wp-ingress
  annotations:
     nginx.ingress.kubernetes.io/rewrite-target: /
spec:
     rules:
       - host: wp.lan
         http:
           paths:
             - path: /
               pathType: Prefix
               backend:
                 service:
                   name: wordpress
                   port:
                     number: 8080
EOF

    minikube kubectl -- apply -f /tmp/wp-ingress.yml
    echo "$(minikube kubectl -- get ingress -o json| jq -r '.items | .[].status.loadBalancer.ingress | .[].ip') wp.lan" | tee -a /etc/hosts


    minikube mount "$(pwd)":/root &

    cat <<EOF> /tmp/wp-cronjob.yml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: wordpress-backup
  namespace: default
spec:
  schedule: "0 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: wordpress-backup
            image: docker.io/caezsar/mysql-backup:0.1
            imagePullPolicy: IfNotPresent
            env:
            - name: MARIADB_HOST
              value: wordpress-mariadb
            - name: MARIADB_PORT_NUMBER
              value: "3306"
            - name: WORDPRESS_DATABASE_NAME
              value: bitnami_wordpress
            - name: WORDPRESS_DATABASE_USER
              value: bn_wordpress
            - name: WORDPRESS_DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: mariadb-password
                  name: wordpress-mariadb
            volumeMounts:
            - mountPath: /backup
              name: wordpress-backup
          volumes:
            - name: wordpress-backup
              hostPath:
                path: "$(pwd)"
                type: Directory
          restartPolicy: OnFailure

EOF

    minikube kubectl -- apply -f /tmp/wp-cronjob.yml
    minikube kubectl -- get cronjobs


echo "Wordpress username: $(helm show values bitnami/wordpress|grep wordpressUsername| awk -F: '{print $2}')"
echo "Wordpress pass: $(minikube kubectl -- get secret --namespace default wordpress -o json | jq -r '.data."wordpress-password"'| base64 -d)"

}


delete_cluster() {
    minikube delete --purge
}


menu() {

    while :; do
    echo "Select option:"
    echo "1) Deploy wordpress cluster"
    echo "2) Destroy wordpress cluster"    
    echo "x) Exit"
    echo -e "\nType your choice (default=1)"

    read -r selected_option    

        case ${selected_option:-1} in
            1) 
                echo "Deploy Wordpress on Minikube...."
                deploy_cluster
                break
            ;;
            2) 
                echo "Destroy Wordpress on Minikube...."
                delete_cluster
                break
            ;;
            x) 
                echo "Exiting..."
                exit
            ;;
            *) 
                echo -e "\nValid options 1-2\n"
                ;;
        esac
    done

}

menu

